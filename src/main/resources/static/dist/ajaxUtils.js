var ajaxUtil = {
	postAjax: function(url,data,callBack){
		$.ajax({
	        "type": "POST",
	        "url": url,
	        "dataType": "json",
	        "data": data,
	        "contentType":"application/json;charset-UTF-8",
	        "success":function(result){
	        callBack(result);
	     },
	        "error":function(jqXHR){
	        alert("发生错误："+ jqXHR.status);
	     }
	    });
	},
	getAjax: function(url,data,callBack){
		$.ajax({
			"type": "GET",
			"url": url,
			"dataType": "json",
			"data": data, //以json格式传递
			"success":function(result){
	        callBack(result);
	      },
			"error":function(jqXHR){
	        alert("发生错误："+ jqXHR.status);
	     }
		});
	}
}