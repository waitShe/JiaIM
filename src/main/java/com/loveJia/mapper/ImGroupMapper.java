package com.loveJia.mapper;

import com.loveJia.entity.ImGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
public interface ImGroupMapper extends BaseMapper<ImGroup> {

}
