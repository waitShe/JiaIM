package com.loveJia.nettyServer.server;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "websocket")
@Data
public class NettyWebSocketProperties {

    private Integer port;
}
