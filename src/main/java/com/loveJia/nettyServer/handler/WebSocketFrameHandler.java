package com.loveJia.nettyServer.handler;

import cn.hutool.core.collection.CollUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.loveJia.entity.ImUser;
import com.loveJia.nettyServer.util.ChannelUtil;
import com.loveJia.nettyServer.util.TypeTransferUtil;
import com.loveJia.po.*;
import com.loveJia.service.IImUserService;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandler.Sharable;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.CloseWebSocketFrame;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.handler.codec.http.websocketx.WebSocketFrame;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

@Slf4j
@Sharable
@Component
public class WebSocketFrameHandler extends SimpleChannelInboundHandler<WebSocketFrame> {
    @Autowired
    private IImUserService iImUserService;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, WebSocketFrame frame) throws Exception {
        if (frame instanceof TextWebSocketFrame) {
            String request = ((TextWebSocketFrame) frame).text();
            log.info("服务器接收到的数据:{}",request);
            String handlerType = JSON.parseObject(request).getString("handlerType");
            ChatMsgDto chatMsgDto = JSON.parseObject(request,TypeTransferUtil.getChatMsg(handlerType));
            if(chatMsgDto instanceof UserDto){
                UserDto userDto = (UserDto) chatMsgDto;
                ChannelUtil.bindChannel(userDto.getUsername(), ctx.channel());
            }
            if(chatMsgDto instanceof SingleChatMsgDto){
                SingleChatMsgDto singleChatMsgDto = (SingleChatMsgDto) chatMsgDto;
                String username = singleChatMsgDto.getToUserName();
                Channel channel = ChannelUtil.getChannel(username);

                QueryWrapper<ImUser> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("USERNAME", singleChatMsgDto.getFromUserName());
                ImUser imUser = iImUserService.getOne(queryWrapper);
                OneOnOneChatMsgDto oneOnOneChatMsgDto = new OneOnOneChatMsgDto();
                oneOnOneChatMsgDto.setId(singleChatMsgDto.getFromUserName());
                oneOnOneChatMsgDto.setUsername(imUser.getNickName());
                oneOnOneChatMsgDto.setContent(singleChatMsgDto.getContent());
                oneOnOneChatMsgDto.setFromid(singleChatMsgDto.getFromUserName());
                oneOnOneChatMsgDto.setTimestamp(new Date().getTime());

                OneOnOneChatMsgDecorateDto oneOnOneChatMsgDecorateDto = new OneOnOneChatMsgDecorateDto();
                oneOnOneChatMsgDecorateDto.setOneOnOneChatMsgDtos(CollUtil.newArrayList(oneOnOneChatMsgDto));
                oneOnOneChatMsgDecorateDto.setEvent("chatMessage");
                channel.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(oneOnOneChatMsgDecorateDto)));
            }
        }
        if(frame instanceof CloseWebSocketFrame){
            ctx.channel().close();
        }
    }
}
