package com.loveJia.nettyServer.util;

import io.netty.channel.Channel;

import java.util.HashMap;
import java.util.Map;

public class ChannelUtil {
    private static final Map<String, Channel> USER_CHANNEL_MAP = new HashMap<>();

    public static Channel getChannel(String userName){
        return USER_CHANNEL_MAP.get(userName);
    }

    public static void bindChannel(String userName,Channel channel){
        USER_CHANNEL_MAP.put(userName, channel);
    }
}
