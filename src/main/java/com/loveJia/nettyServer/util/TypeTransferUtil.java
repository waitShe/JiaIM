package com.loveJia.nettyServer.util;

import cn.hutool.core.collection.CollUtil;
import com.loveJia.po.ChatMsgDto;
import com.loveJia.po.SingleChatMsgDto;
import com.loveJia.po.UserDto;

import java.util.Map;

public class TypeTransferUtil {

    private static Map<String,Class<? extends ChatMsgDto>> TYPE_CLASS = CollUtil.newHashMap();

    static {
        TYPE_CLASS.put("0", UserDto.class);
        TYPE_CLASS.put("1", SingleChatMsgDto.class);
    }

    public static Class<? extends ChatMsgDto> getChatMsg(String type){
        return TYPE_CLASS.get(type);
    }
}
