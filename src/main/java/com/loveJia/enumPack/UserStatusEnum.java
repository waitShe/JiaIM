package com.loveJia.enumPack;

import java.util.Arrays;
import java.util.stream.Collectors;

public enum UserStatusEnum {
    ONLINE("1","online","在线"),
    HIDE("2","hide","离线");

    UserStatusEnum(String value,String name,String desc){
        this.value = value;
        this.name = name;
        this.desc = desc;
    }

    public static String getValue(String name){
        return Arrays.stream(UserStatusEnum.values()).filter(userStatusEnum -> userStatusEnum.name.equals(name))
                .collect(Collectors.toList()).get(0).getValue();
    }

    public static String getName(String value){
        return Arrays.stream(UserStatusEnum.values()).filter(userStatusEnum -> userStatusEnum.value.equals(value))
                .collect(Collectors.toList()).get(0).getName();
    }

    public static String getDesc(String name){
        return Arrays.stream(UserStatusEnum.values()).filter(userStatusEnum -> userStatusEnum.name.equals(name))
                .collect(Collectors.toList()).get(0).getDesc();
    }

    private String value;

    private String name;

    private String desc;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public String getDesc() {
        return desc;
    }
}
