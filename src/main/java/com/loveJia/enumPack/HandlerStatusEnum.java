package com.loveJia.enumPack;

public enum HandlerStatusEnum {
    UN_HANDLER("0","未处理"),

    ALREADY_HANDLER("1","已处理");

    HandlerStatusEnum(String value,String name){
        this.value = value;
        this.name = name;
    }

    private String value;

    private String name;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
