package com.loveJia.enumPack;

public enum MessageStatusEnum {
    UN_READ("0","未读"),

    ALREADY_READ("1","已读");

    MessageStatusEnum(String value,String name){
        this.value = value;
        this.name = name;
    }

    private String value;

    private String name;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
