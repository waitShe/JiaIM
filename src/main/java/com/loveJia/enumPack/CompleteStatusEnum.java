package com.loveJia.enumPack;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Filter;

import java.util.List;

public enum CompleteStatusEnum {
    UN_COMPLETE("0","未处理"),

    UN_REFUSE("2","已拒绝"),

    ALREADY_COMPLETE("1","已通过");

    CompleteStatusEnum(String value,String name){
        this.value = value;
        this.name = name;
    }

    private String value;

    private String name;

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }

    public static String getName(String value){
        List<CompleteStatusEnum> completeStatusEnums = CollUtil.newArrayList(CompleteStatusEnum.values());
        CompleteStatusEnum statusEnum = CollUtil.findOne(completeStatusEnums, new Filter<CompleteStatusEnum>() {
            @Override
            public boolean accept(CompleteStatusEnum completeStatusEnum) {
                return value.equals(completeStatusEnum.getValue());
            }
        });
        return statusEnum.getName();
    }
}
