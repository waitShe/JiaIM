package com.loveJia.util;

import cn.hutool.core.collection.CollUtil;
import com.loveJia.po.ResultDto;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.groups.Default;
import java.util.Iterator;
import java.util.Set;

public class ValidatorUtils {
    private static Validator validator;

    static {
        validator = Validation.buildDefaultValidatorFactory().getValidator();
    }

    /**
     * 校验对象
     * @param object        待校验对象
     */
    public static ResultDto validateEntity(Object object) {
        ResultDto resultDto = new ResultDto();
        Set<ConstraintViolation<Object>> constraintViolations = validator.validate(object, new Class[]{Default.class});
        if(CollUtil.isEmpty(constraintViolations)){
            resultDto.setSuccess(true);
            return resultDto;
        }
        Iterator<ConstraintViolation<Object>> objectIterator = constraintViolations.iterator();
        ConstraintViolation<Object> objectConstraintViolation = objectIterator.next();
        resultDto.setSuccess(false);
        resultDto.setMsg(objectConstraintViolation.getMessage());
        return resultDto;
    }
}
