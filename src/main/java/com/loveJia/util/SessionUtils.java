package com.loveJia.util;

import com.loveJia.constant.GlobalConstant;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;

public class SessionUtils {
    public static String getUsername(){
        Subject subject = SecurityUtils.getSubject();
        Object name = subject.getSession().getAttribute(GlobalConstant.USER_NAME);
        return name == null?null:name.toString();
    }

    public static Integer getUserId(){
        Subject subject = SecurityUtils.getSubject();
        Object name = subject.getSession().getAttribute(GlobalConstant.USER_ID);
        return name == null?null:(Integer) name;
    }
}
