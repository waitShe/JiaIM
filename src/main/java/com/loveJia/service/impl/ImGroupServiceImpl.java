package com.loveJia.service.impl;

import com.loveJia.entity.ImGroup;
import com.loveJia.mapper.ImGroupMapper;
import com.loveJia.service.IImGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Service
public class ImGroupServiceImpl extends ServiceImpl<ImGroupMapper, ImGroup> implements IImGroupService {

}
