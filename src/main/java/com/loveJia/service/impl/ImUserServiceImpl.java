package com.loveJia.service.impl;

import com.loveJia.entity.ImUser;
import com.loveJia.mapper.ImUserMapper;
import com.loveJia.service.IImUserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Service
public class ImUserServiceImpl extends ServiceImpl<ImUserMapper, ImUser> implements IImUserService {

}
