package com.loveJia.service.impl;

import com.loveJia.entity.ImAddFriendsMsg;
import com.loveJia.mapper.ImAddFriendsMsgMapper;
import com.loveJia.service.IImAddFriendsMsgService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Service
public class ImAddFriendsMsgServiceImpl extends ServiceImpl<ImAddFriendsMsgMapper, ImAddFriendsMsg> implements IImAddFriendsMsgService {

}
