package com.loveJia.service.impl;

import com.loveJia.entity.ImFriends;
import com.loveJia.mapper.ImFriendsMapper;
import com.loveJia.service.IImFriendsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Service
public class ImFriendsServiceImpl extends ServiceImpl<ImFriendsMapper, ImFriends> implements IImFriendsService {

}
