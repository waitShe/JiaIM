package com.loveJia.service;

import com.loveJia.entity.ImUser;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
public interface IImUserService extends IService<ImUser> {

}
