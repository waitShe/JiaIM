package com.loveJia.constant;

public class GlobalConstant {
    public static final String SESSION_AUTH_USER_INFO = "sessionAuthUserInfo";

    public static final String USER_NAME = "userName";

    public static final String USER_ID = "userId";
}
