package com.loveJia.po;

import lombok.Data;

@Data
public class AddNewFriendsMsgDto {
    private Integer groupId;

    private String remake;

    private Integer toUserId;
}
