package com.loveJia.po;

import lombok.Data;

@Data
public class OneOnOneChatMsgDto{
    private String username;
    private String avatar;
    private String id;
    private String type = "friend";
    private String content;
    private Integer cid = 0;
    private Boolean mine = false;
    private String fromid;
    private Long timestamp;
}
