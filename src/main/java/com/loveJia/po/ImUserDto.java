package com.loveJia.po;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ImUserDto {
    /**
     * 主键
     */
    private Integer id;

    /**
     * 账号
     */
    private String username;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 密码
     */
    private String password;

    /**
     * 性别
     */
    private String sex;

    /**
     * 签名
     */
    private String sign;

    /**
     * 状态
     */
    private String status;

    /**
     * 头像
     */
    private String headImg;

    /**
     * 创建时间
     */
    private LocalDateTime createTime;
}
