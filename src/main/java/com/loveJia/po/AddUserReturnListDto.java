package com.loveJia.po;

import lombok.Data;

import java.util.List;

@Data
public class AddUserReturnListDto extends MessageDtoSupper{

    private List<AddUserReturnDto> userReturnDtos;
}
