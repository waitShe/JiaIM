package com.loveJia.po;

import lombok.Data;

@Data
public class MsgBoxCountDto extends MessageDtoSupper{

    private Integer count;
}
