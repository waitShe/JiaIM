package com.loveJia.po;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserDto extends ChatMsgDto{
    @NotNull(message = "用户名不能为空")
    private String username;

    @NotNull(message = "密码不能为空")
    private String password;

    private String nickName;
}
