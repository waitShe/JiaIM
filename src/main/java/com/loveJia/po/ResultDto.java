package com.loveJia.po;

import lombok.Data;

@Data
public class ResultDto {
    public ResultDto() {
    }

    public ResultDto(Boolean success, String msg) {
        this.success = success;
        this.msg = msg;
    }

    public ResultDto(Boolean success, String msg, Object data) {
        this.success = success;
        this.msg = msg;
        this.data = data;
    }

    private Boolean success;

    private String msg;

    private Object data;
}
