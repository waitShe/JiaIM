package com.loveJia.po;

import lombok.Data;

@Data
public class AddUserReturnDto{

    /**
     * avatar : a.jpg
     * username : 好友昵称
     * groupid : 2
     * id : 1233333312121212
     * sign : 的工作
     */

    private String type;
    private String avatar;
    private String username;
    private Integer groupid;
    private String id;
    private String sign;
}
