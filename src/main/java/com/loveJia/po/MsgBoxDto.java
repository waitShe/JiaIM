package com.loveJia.po;

import java.util.List;

public class MsgBoxDto {

    /**
     * code : 0
     * pages : 1
     * data : [{"id":76,"content":"申请添加你为好友","uid":168,"from":166488,"from_group":0,"type":1,"remark":"有问题要问","href":null,"read":1,"time":"刚刚","user":{"id":166488,"avatar":"http://q.qlogo.cn/qqapp/101235792/B704597964F9BD0DB648292D1B09F7E8/100","username":"李彦宏","sign":null}},{"id":75,"content":"申请添加你为好友","uid":168,"from":347592,"from_group":0,"type":1,"remark":"你好啊！","href":null,"read":1,"time":"刚刚","user":{"id":347592,"avatar":"http://q.qlogo.cn/qqapp/101235792/B78751375E0531675B1272AD994BA875/100","username":"麻花疼","sign":null}},{"id":62,"content":"雷军 拒绝了你的好友申请","uid":168,"from":null,"from_group":null,"type":1,"remark":null,"href":null,"read":1,"time":"10天前","user":{"id":null}},{"id":60,"content":"马小云 已经同意你的好友申请","uid":168,"from":null,"from_group":null,"type":1,"remark":null,"href":null,"read":1,"time":"10天前","user":{"id":null}},{"id":61,"content":"贤心 已经同意你的好友申请","uid":168,"from":null,"from_group":null,"type":1,"remark":null,"href":null,"read":1,"time":"10天前","user":{"id":null}}]
     */

    private int code;
    private int pages;
    private String emit;
    private List<DataBean> data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public String getEmit() {
        return emit;
    }

    public void setEmit(String emit) {
        this.emit = emit;
    }

    public static class DataBean {
        /**
         * id : 76
         * content : 申请添加你为好友
         * uid : 168
         * from : 166488
         * from_group : 0
         * type : 1
         * remark : 有问题要问
         * href : null
         * read : 1
         * time : 刚刚
         * user : {"id":166488,"avatar":"http://q.qlogo.cn/qqapp/101235792/B704597964F9BD0DB648292D1B09F7E8/100","username":"李彦宏","sign":null}
         */

        private int id;
        private String content = "申请添加你为好友";
        private int uid = 168;
        private int from;
        private int from_group = 0;
        private int type = 1;
        private String remark;
        private Object href;
        private int read = 1;
        private String time;
        private UserBean user;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public int getUid() {
            return uid;
        }

        public void setUid(int uid) {
            this.uid = uid;
        }

        public int getFrom() {
            return from;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public int getFrom_group() {
            return from_group;
        }

        public void setFrom_group(int from_group) {
            this.from_group = from_group;
        }

        public int getType() {
            return type;
        }

        public void setType(int type) {
            this.type = type;
        }

        public String getRemark() {
            return remark;
        }

        public void setRemark(String remark) {
            this.remark = remark;
        }

        public Object getHref() {
            return href;
        }

        public void setHref(Object href) {
            this.href = href;
        }

        public int getRead() {
            return read;
        }

        public void setRead(int read) {
            this.read = read;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public UserBean getUser() {
            return user;
        }

        public void setUser(UserBean user) {
            this.user = user;
        }

        public static class UserBean {
            /**
             * id : 166488
             * avatar : http://q.qlogo.cn/qqapp/101235792/B704597964F9BD0DB648292D1B09F7E8/100
             * username : 李彦宏
             * sign : null
             */

            private int id;
            private String avatar;
            private String username;
            private Object sign;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getAvatar() {
                return avatar;
            }

            public void setAvatar(String avatar) {
                this.avatar = avatar;
            }

            public String getUsername() {
                return username;
            }

            public void setUsername(String username) {
                this.username = username;
            }

            public Object getSign() {
                return sign;
            }

            public void setSign(Object sign) {
                this.sign = sign;
            }
        }
    }
}
