package com.loveJia.po;

import lombok.Data;

@Data
public class AgreeRefuseDto {

    private Integer fromUserId;

    private Integer fromGroupId;

    private Integer groupId;

    private Integer friendsMsgId;
}
