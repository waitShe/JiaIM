package com.loveJia.po;

import lombok.Data;

@Data
public class SingleChatMsgDto extends ChatMsgDto{
    private String fromUserName;

    private String content;

    private String toUserName;
}
