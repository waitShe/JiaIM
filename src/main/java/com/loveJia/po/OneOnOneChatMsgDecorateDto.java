package com.loveJia.po;

import lombok.Data;

import java.util.List;

@Data
public class OneOnOneChatMsgDecorateDto extends MessageDtoSupper{

    List<OneOnOneChatMsgDto> oneOnOneChatMsgDtos;
}
