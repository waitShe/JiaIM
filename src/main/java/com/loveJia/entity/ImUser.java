package com.loveJia.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ImUser implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 账号
     */
    @TableField("USERNAME")
    private String username;

    /**
     * 昵称
     */
    @TableField("NICK_NAME")
    private String nickName;

    /**
     * 密码
     */
    @TableField("PASSWORD")
    private String password;

    /**
     * 性别
     */
    @TableField("SEX")
    private String sex;

    /**
     * 签名
     */
    @TableField("SIGN")
    private String sign;

    /**
     * 状态
     */
    @TableField("STATUS")
    private String status;

    /**
     * 头像
     */
    @TableField("HEAD_IMG")
    private String headImg;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;


}
