package com.loveJia.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ImAddFriendsMsg implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 验证信息
     */
    @TableField("REMAKE")
    private String remake;

    /**
     * 消息状态(0：未读,1：已读)
     */
    @TableField("STATUS")
    private String status;

    /**
     * 申请时间
     */
    @TableField("POST_TIME")
    private LocalDateTime postTime;

    /**
     * 发送用户ID
     */
    @TableField("FROM_USER_ID")
    private Integer fromUserId;

    /**
     * 接收用户ID
     */
    @TableField("TO_USER_ID")
    private Integer toUserId;

    /**
     * 所属分组ID
     */
    @TableField("GROUP_ID")
    private Integer groupId;

    /**
     * 操作状态
     */
    @TableField("COMPLETE_STATUS")
    private String completeStatus;

    /**
     * 处理状态
     */
    @TableField("HANDLER_STATUS")
    private String handlerStatus;
}
