package com.loveJia.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
public class ImFriends implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(value = "ID", type = IdType.AUTO)
    private Integer id;

    /**
     * 好友ID
     */
    @TableField("FRIEND_ID")
    private Integer friendId;

    /**
     * 用户ID
     */
    @TableField("OWN_ID")
    private Integer ownId;

    /**
     * 好友备注
     */
    @TableField("REMAKES")
    private String remakes;

    /**
     * 所属分组ID
     */
    @TableField("GROUP_ID")
    private Integer groupId;

    /**
     * 创建时间
     */
    @TableField("CREATE_TIME")
    private LocalDateTime createTime;


}
