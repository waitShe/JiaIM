package com.loveJia.shiro;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.servlet.AdviceFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Slf4j
public class AjaxFilter extends AdviceFilter {
    @Override
    protected boolean preHandle(ServletRequest req, ServletResponse resp) throws Exception {
        HttpServletRequest request = (HttpServletRequest)req;
        HttpServletResponse response = (HttpServletResponse)resp;
        //获取请求头中的信息，ajax请求最大的特点就是请求头中多了 X-Requested-With 参数
        String requestType = request.getHeader("X-Requested-With");
        if("XMLHttpRequest".equals(requestType)){
            log.info("本次请求是AJAX请求!");
            //现在的用途就是判断当前用户是否被认证
            //先获取到由Web容器管理的subject对象
            Subject subject = SecurityUtils.getSubject();
            //判断是否已经认证
            boolean isAuthc = subject.isAuthenticated();
            if(!isAuthc){
                if(request.getRequestURI().contains("loginUser") || request.getRequestURI().contains("/user/addUser")){
                    return true;
                }
                log.info("当前账户使用Shiro认证失败!");
                //如果当前账户没有被认证,本次请求被驳回，ajax进入error的function中进行重定向到登录界面。
                return false;
            }
            return true;
        }else{
            //如果请求类型不是ajax，那么此时requestType为null
            log.info("非AJAX请求!");
        }
        //默认返回的是true，将本次请求继续执行下去
        return super.preHandle(request, response);
    }
}
