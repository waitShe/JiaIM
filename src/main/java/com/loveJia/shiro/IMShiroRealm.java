package com.loveJia.shiro;

import cn.hutool.crypto.digest.MD5;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.loveJia.constant.GlobalConstant;
import com.loveJia.entity.ImUser;
import com.loveJia.enumPack.UserStatusEnum;
import com.loveJia.service.IImUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;

public class IMShiroRealm extends AuthorizingRealm {
    @Autowired
    private IImUserService iImUserService;


    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return new SimpleAuthorizationInfo();
    }

    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken usernamePasswordToken = (UsernamePasswordToken) authenticationToken;
        String userName = usernamePasswordToken.getUsername();
        String password = MD5.create().digestHex(String.valueOf(usernamePasswordToken.getPassword()), "UTF-8");
        usernamePasswordToken.setPassword(password.toCharArray());
        QueryWrapper<ImUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("USERNAME", userName);
        queryWrapper.eq("PASSWORD", password);
        ImUser imUser = iImUserService.getOne(queryWrapper);
        if(imUser == null){
            throw new UnknownAccountException("用户不存在");
        }
        imUser.setStatus(UserStatusEnum.ONLINE.getValue());
        iImUserService.updateById(imUser);
        Subject subject = SecurityUtils.getSubject();
        subject.getSession().setAttribute(GlobalConstant.SESSION_AUTH_USER_INFO, SecurityUtils.getSubject().getPrincipal());
        subject.getSession().setAttribute(GlobalConstant.USER_NAME, imUser.getUsername());
        subject.getSession().setAttribute(GlobalConstant.USER_ID, imUser.getId());
        return new SimpleAuthenticationInfo(imUser,imUser.getPassword(),imUser.getUsername());
    }
}
