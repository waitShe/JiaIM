package com.loveJia.exception;

import com.loveJia.po.ResultDto;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalException {
    @ExceptionHandler(value = BusinessException.class)
    @ResponseBody
    public ResultDto handle(BusinessException e){
        return new ResultDto(false,e.getMessage());
    }
}
