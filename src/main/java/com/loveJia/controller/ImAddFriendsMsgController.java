package com.loveJia.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.loveJia.entity.ImAddFriendsMsg;
import com.loveJia.entity.ImFriends;
import com.loveJia.entity.ImUser;
import com.loveJia.enumPack.CompleteStatusEnum;
import com.loveJia.enumPack.HandlerStatusEnum;
import com.loveJia.enumPack.MessageStatusEnum;
import com.loveJia.nettyServer.util.ChannelUtil;
import com.loveJia.po.*;
import com.loveJia.service.IImAddFriendsMsgService;
import com.loveJia.service.IImFriendsService;
import com.loveJia.service.IImUserService;
import com.loveJia.util.SessionUtils;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Controller
@RequestMapping("/addFriendsMsg")
public class ImAddFriendsMsgController {

    @Autowired
    private IImAddFriendsMsgService iImAddFriendsMsgService;

    @Autowired
    private IImUserService iImUserService;

    @Autowired
    private IImFriendsService iImFriendsService;

    @RequestMapping("/addNew")
    @ResponseBody
    public ResultDto addNew(@RequestBody AddNewFriendsMsgDto addNewFriendsMsgDto){
        ImAddFriendsMsg imAddFriendsMsg = new ImAddFriendsMsg();
        BeanUtil.copyProperties(addNewFriendsMsgDto,imAddFriendsMsg);
        imAddFriendsMsg.setFromUserId(SessionUtils.getUserId());
        imAddFriendsMsg.setPostTime(LocalDateTime.now());
        imAddFriendsMsg.setStatus(MessageStatusEnum.UN_READ.getValue());
        imAddFriendsMsg.setCompleteStatus(CompleteStatusEnum.UN_COMPLETE.getValue());
        imAddFriendsMsg.setHandlerStatus(HandlerStatusEnum.UN_HANDLER.getValue());
        iImAddFriendsMsgService.save(imAddFriendsMsg);

        // 如果申请添加的好友channel存在
        ImUser friendUser = iImUserService.getById(addNewFriendsMsgDto.getToUserId());
        if(friendUser != null){
            Channel channel = ChannelUtil.getChannel(friendUser.getUsername());
            if(channel != null){
                QueryWrapper<ImAddFriendsMsg> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("TO_USER_ID", friendUser.getId());
                queryWrapper.eq("STATUS", MessageStatusEnum.UN_READ.getValue());
                List<ImAddFriendsMsg> imMessages = iImAddFriendsMsgService.list(queryWrapper);
                MsgBoxCountDto msgBoxCountDto = new MsgBoxCountDto();
                msgBoxCountDto.setEvent("count");
                msgBoxCountDto.setCount(imMessages.size());
                channel.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(msgBoxCountDto)));
            }
        }
        return new ResultDto(true, "申请成功");
    }

    @RequestMapping("/msgbox")
    public String msgBox(){
        return "msgbox";
    }

    @RequestMapping("/msgBoxData")
    @ResponseBody
    public MsgBoxDto msgBoxData(){
        Integer userId = SessionUtils.getUserId();
        QueryWrapper<ImAddFriendsMsg> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("TO_USER_ID",userId);
        // queryWrapper.eq("STATUS", MessageStatusEnum.UN_READ.getValue());
        queryWrapper.eq("HANDLER_STATUS", HandlerStatusEnum.UN_HANDLER.getValue());
        List<ImAddFriendsMsg> imAddFriendsMsgs = iImAddFriendsMsgService.list(queryWrapper);
        return toMsgBoxDto(imAddFriendsMsgs);
    }

    @RequestMapping("/resetMsgAlready")
    @ResponseBody
    public ResultDto resetMsgAlready(@RequestBody List<Integer> friendsIdList){
        if(CollUtil.isEmpty(friendsIdList)){
            return new ResultDto(true, "成功");
        }
        QueryWrapper<ImAddFriendsMsg> queryWrapper = new QueryWrapper<>();
        queryWrapper.in("ID",friendsIdList);
        List<ImAddFriendsMsg> imAddFriendsMsgs = iImAddFriendsMsgService.list(queryWrapper);
        for(ImAddFriendsMsg imAddFriendsMsg : imAddFriendsMsgs){
            imAddFriendsMsg.setStatus(MessageStatusEnum.ALREADY_READ.getValue());
        }
        iImAddFriendsMsgService.updateBatchById(imAddFriendsMsgs);
        return new ResultDto(true, "成功");
    }

    @RequestMapping("/agreeAddFriends")
    @ResponseBody
    public ResultDto agreeAddFriends(@RequestBody AgreeRefuseDto agreeRefuseDto){
        if(agreeRefuseDto.getFromGroupId() == null){
            return new ResultDto(false, "fromGroupId不存在");
        }
        // 申请人
        ImFriends fromFriends = new ImFriends();
        fromFriends.setFriendId(SessionUtils.getUserId());
        fromFriends.setOwnId(agreeRefuseDto.getFromUserId());
        fromFriends.setGroupId(agreeRefuseDto.getFromGroupId());
        fromFriends.setCreateTime(LocalDateTime.now());
        iImFriendsService.save(fromFriends);

        // 添加人
        ImFriends toFriends = new ImFriends();
        toFriends.setFriendId(agreeRefuseDto.getFromUserId());
        toFriends.setOwnId(SessionUtils.getUserId());
        toFriends.setGroupId(agreeRefuseDto.getGroupId());
        toFriends.setCreateTime(LocalDateTime.now());
        iImFriendsService.save(toFriends);

        ImAddFriendsMsg imAddFriendsMsg = iImAddFriendsMsgService.getById(agreeRefuseDto.getFriendsMsgId());
        imAddFriendsMsg.setCompleteStatus(CompleteStatusEnum.ALREADY_COMPLETE.getValue());
        imAddFriendsMsg.setHandlerStatus(HandlerStatusEnum.ALREADY_HANDLER.getValue());
        iImAddFriendsMsgService.updateById(imAddFriendsMsg);

        // 通过后给申请人发送消息
        ImUser applyUser = iImUserService.getById(agreeRefuseDto.getFromUserId());

        ImUser toUser = iImUserService.getById(SessionUtils.getUserId());
        if(applyUser != null){
            Channel channel = ChannelUtil.getChannel(applyUser.getUsername());
            if(channel != null){
                AddUserReturnListDto addUserReturnListDto = new AddUserReturnListDto();
                addUserReturnListDto.setEvent("addList");

                AddUserReturnDto userReturnDto = new AddUserReturnDto();
                userReturnDto.setType("friend");
                userReturnDto.setAvatar(toUser.getHeadImg());
                userReturnDto.setGroupid(agreeRefuseDto.getFromGroupId());
                userReturnDto.setUsername(toUser.getNickName());
                userReturnDto.setId(toUser.getUsername());
                addUserReturnListDto.setUserReturnDtos(CollUtil.newArrayList(userReturnDto));
                channel.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(addUserReturnListDto)));
            }
        }
        return new ResultDto(true, "添加通过");
    }

    @RequestMapping("/refuseAddFriends")
    @ResponseBody
    public ResultDto refuseAddFriends(@RequestBody AgreeRefuseDto agreeRefuseDto){
        ImAddFriendsMsg imAddFriendsMsg = new ImAddFriendsMsg();
        imAddFriendsMsg.setToUserId(agreeRefuseDto.getFromUserId());
        imAddFriendsMsg.setPostTime(LocalDateTime.now());
        imAddFriendsMsg.setFromUserId(SessionUtils.getUserId());
        imAddFriendsMsg.setFromUserId(SessionUtils.getUserId());
        imAddFriendsMsg.setPostTime(LocalDateTime.now());
        imAddFriendsMsg.setStatus(MessageStatusEnum.UN_READ.getValue());
        imAddFriendsMsg.setCompleteStatus(CompleteStatusEnum.UN_REFUSE.getValue());
        iImAddFriendsMsgService.save(imAddFriendsMsg);

        ImAddFriendsMsg addFriendsMsg = iImAddFriendsMsgService.getById(agreeRefuseDto.getFriendsMsgId());
        addFriendsMsg.setCompleteStatus(CompleteStatusEnum.UN_REFUSE.getValue());
        addFriendsMsg.setHandlerStatus(HandlerStatusEnum.ALREADY_HANDLER.getValue());
        iImAddFriendsMsgService.updateById(addFriendsMsg);
        return new ResultDto(true, "已拒绝");
    }


    public MsgBoxDto toMsgBoxDto(List<ImAddFriendsMsg> imAddFriendsMsgs){
        MsgBoxDto msgBoxDto = new MsgBoxDto();
        msgBoxDto.setCode(0);
        msgBoxDto.setPages(1);

        List<MsgBoxDto.DataBean> dataBeans = new ArrayList<>();
        for(ImAddFriendsMsg friendsMsg : imAddFriendsMsgs){
            MsgBoxDto.DataBean dataBean = new MsgBoxDto.DataBean();
            dataBean.setId(friendsMsg.getId());
            dataBean.setUid(friendsMsg.getFromUserId());
            if(CompleteStatusEnum.UN_COMPLETE.getValue().equals(friendsMsg.getCompleteStatus())){
                dataBean.setRemark(friendsMsg.getRemake());
                dataBean.setFrom(friendsMsg.getFromUserId());
                dataBean.setFrom_group(friendsMsg.getGroupId());
            }
            dataBean.setTime(DateUtil.formatDateTime(Date.from(friendsMsg.getPostTime().atZone(ZoneId.systemDefault()).toInstant())));

            ImUser friendUser = iImUserService.getById(friendsMsg.getFromUserId());
            if(CompleteStatusEnum.UN_COMPLETE.getValue().equals(friendsMsg.getCompleteStatus())) {
                MsgBoxDto.DataBean.UserBean userBean = new MsgBoxDto.DataBean.UserBean();
                userBean.setId(friendUser.getId());
                userBean.setUsername(friendUser.getNickName());
                userBean.setAvatar(friendUser.getHeadImg());
                userBean.setSign(friendUser.getSign());
                dataBean.setUser(userBean);
            }else{
                dataBean.setContent(friendUser.getNickName() + CompleteStatusEnum.getName(friendsMsg.getCompleteStatus()) + "您的请求!");
            }
            dataBeans.add(dataBean);
        }
        msgBoxDto.setData(dataBeans);
        return msgBoxDto;
    }
}
