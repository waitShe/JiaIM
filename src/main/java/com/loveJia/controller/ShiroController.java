package com.loveJia.controller;

import com.loveJia.po.ResultDto;
import com.loveJia.po.UserDto;
import com.loveJia.service.IImUserService;
import com.loveJia.util.SessionUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class ShiroController {
    @Autowired
    private IImUserService iImUserService;

    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    @RequestMapping("/unauthorized")
    public String unauthorized(){
        return "unauthorized";
    }

    @RequestMapping("/register")
    public String register(){
        return "register";
    }

    @RequestMapping("/loginUser")
    @ResponseBody
    public ResultDto loginUser(@RequestBody UserDto userDto){
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(userDto.getUsername(), userDto.getPassword());
        try {
            subject.login(token);
        }catch (UnknownAccountException e){
            return new ResultDto(false, "账号或者密码错误");
        }
        return new ResultDto(true, "登录成功");
    }

    @RequestMapping("/index")
    public String index(Model model){
        model.addAttribute("userName", SessionUtils.getUsername());
        return "index";
    }
}
