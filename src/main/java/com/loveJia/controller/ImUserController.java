package com.loveJia.controller;


import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.digest.MD5;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.loveJia.entity.ImAddFriendsMsg;
import com.loveJia.entity.ImFriends;
import com.loveJia.entity.ImGroup;
import com.loveJia.entity.ImUser;
import com.loveJia.enumPack.MessageStatusEnum;
import com.loveJia.enumPack.UserStatusEnum;
import com.loveJia.nettyServer.util.ChannelUtil;
import com.loveJia.po.*;
import com.loveJia.service.IImAddFriendsMsgService;
import com.loveJia.service.IImFriendsService;
import com.loveJia.service.IImGroupService;
import com.loveJia.service.IImUserService;
import com.loveJia.util.SessionUtils;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author Nameless
 * @since 2019-11-12
 */
@Controller
@RequestMapping("/user")
public class ImUserController {
    @Autowired
    private IImUserService iImUserService;

    @Autowired
    private IImGroupService iImGroupService;

    @Autowired
    private IImFriendsService iImFriendsService;

    @Autowired
    private IImAddFriendsMsgService iImAddFriendsMsgService;

    @RequestMapping("/addUser")
    @ResponseBody
    public ResultDto addUser(@RequestBody UserDto userDto){
        ImUser imUser = new ImUser();
        BeanUtil.copyProperties(userDto, imUser);
        String password = MD5.create().digestHex(userDto.getPassword(), "UTF-8");
        imUser.setStatus(UserStatusEnum.HIDE.getValue());
        imUser.setPassword(password);
        imUser.setHeadImg("/dist/images/timg.jpg");
        imUser.setCreateTime(LocalDateTime.now());
        iImUserService.save(imUser);

        ImGroup imFriendGroup = new ImGroup();
        imFriendGroup.setGroupName("我的好友");
        imFriendGroup.setUserId(imUser.getId());
        imFriendGroup.setCreateTime(LocalDateTime.now());
        iImGroupService.save(imFriendGroup);

        ImFriends imFriends = new ImFriends();
        imFriends.setFriendId(imUser.getId());
        imFriends.setOwnId(imUser.getId());
        imFriends.setGroupId(imFriendGroup.getId());
        imFriends.setCreateTime(LocalDateTime.now());
        iImFriendsService.save(imFriends);
        return new ResultDto(true, "注册成功");
    }

    @RequestMapping("/getUserInfo")
    @ResponseBody
    public ImUserInfoDto getUserInfo(HttpServletRequest request){
        String path = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
        String username = SessionUtils.getUsername();
        QueryWrapper<ImUser> wrapper = new QueryWrapper<>();
        wrapper.eq("USERNAME", username);
        ImUser imUser = iImUserService.getOne(wrapper);

        QueryWrapper<ImGroup> groupQueryWrapper = new QueryWrapper<>();
        groupQueryWrapper.eq("USER_ID", imUser.getId());
        List<ImGroup> imGroups = iImGroupService.list(groupQueryWrapper);

        QueryWrapper<ImFriends> friendsQueryWrapper = new QueryWrapper<>();
        friendsQueryWrapper.eq("OWN_ID", imUser.getId());

        List<ImFriends> imFriends = iImFriendsService.list(friendsQueryWrapper);


        ImUserInfoDto.DataBean.MineBean mineBean = new ImUserInfoDto.DataBean.MineBean();
        mineBean.setId(imUser.getUsername());
        mineBean.setUsername(imUser.getNickName());
        mineBean.setSign(imUser.getSign());
        mineBean.setStatus(UserStatusEnum.getName(imUser.getStatus()));
        mineBean.setAvatar(path + imUser.getHeadImg());

        ImUserInfoDto.DataBean dataBean = new ImUserInfoDto.DataBean();
        dataBean.setMine(mineBean);

        List<ImUserInfoDto.DataBean.FriendBean> friendBeans = new ArrayList<>();
        for(ImGroup imFriendGroup : imGroups){
            ImUserInfoDto.DataBean.FriendBean friendBean = new ImUserInfoDto.DataBean.FriendBean();
            friendBean.setGroupname(imFriendGroup.getGroupName());
            List<ImFriends> imFriendsGroupList = imFriends.stream().filter(imFriend -> imFriend.getGroupId().equals(imFriendGroup.getId()))
                    .collect(Collectors.toList());
            List<ImUserInfoDto.DataBean.FriendBean.ListBean> listBeans = new ArrayList<>();
            imFriendsGroupList.stream().forEach(imFriendsGroup -> {
                ImUser friendUser = iImUserService.getById(imFriendsGroup.getFriendId());
                ImUserInfoDto.DataBean.FriendBean.ListBean listBean = new ImUserInfoDto.DataBean.FriendBean.ListBean();
                listBean.setId(friendUser.getUsername());
                listBean.setStatus(UserStatusEnum.getName(friendUser.getStatus()));
                listBean.setUsername(friendUser.getNickName());
                listBean.setSign(friendUser.getSign());
                listBean.setAvatar(path + friendUser.getHeadImg());
                listBeans.add(listBean);
            });
            friendBean.setList(listBeans);
            friendBean.setId(imFriendGroup.getId());
            friendBeans.add(friendBean);
        }
        dataBean.setFriend(friendBeans);

        ImUserInfoDto imUserInfoDto = new ImUserInfoDto();
        imUserInfoDto.setCode(0);
        imUserInfoDto.setMsg("成功");
        imUserInfoDto.setData(dataBean);
        return imUserInfoDto;
    }

    @RequestMapping("/getBoxCount")
    @ResponseBody
    public ResultDto getBoxCount(){
        QueryWrapper<ImAddFriendsMsg> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("TO_USER_ID", SessionUtils.getUserId());
        queryWrapper.eq("STATUS", MessageStatusEnum.UN_READ.getValue());
        List<ImAddFriendsMsg> imMessages = iImAddFriendsMsgService.list(queryWrapper);
        if(CollUtil.isNotEmpty(imMessages)){
            Channel channel = ChannelUtil.getChannel(SessionUtils.getUsername());
            if(channel != null){
                MsgBoxCountDto msgBoxCountDto = new MsgBoxCountDto();
                msgBoxCountDto.setEvent("count");
                msgBoxCountDto.setCount(imMessages.size());
                channel.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(msgBoxCountDto)));
            }
        }
        return new ResultDto(true, "查询消息数量成功");
    }

    @RequestMapping("/find")
    public String find(HttpServletRequest request, Model model){
        String path = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort();
        model.addAttribute("path",path);
        return "find";
    }

    @RequestMapping("/findUser/{account}")
    @ResponseBody
    public ResultDto findUser(@PathVariable(value = "account") String account){
        if(StrUtil.isBlank(account)){
            return new ResultDto(false, "请输入查询的账号");
        }
        QueryWrapper<ImUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("USERNAME", account);
        List<ImUser> imUsers = iImUserService.list(queryWrapper);
        List<ImUserDto> imUserDtos = new ArrayList<>();
        for(ImUser imUser : imUsers){
            ImUserDto imUserDto = new ImUserDto();
            BeanUtil.copyProperties(imUser,imUserDto);
            imUserDtos.add(imUserDto);
        }
        return new ResultDto(true, "查询成功", imUserDtos);
    }
}
