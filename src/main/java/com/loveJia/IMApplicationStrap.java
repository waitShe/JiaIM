package com.loveJia;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.loveJia.mapper")
public class IMApplicationStrap {
    public static void main(String[] args) {
        SpringApplication.run(IMApplicationStrap.class,args);
    }
}
